import React from 'react';
import { GeoJSON, FeatureGroup, Popup } from 'react-leaflet';
import "../css/GeojsonLayer.css"
import { CircleMarker } from 'react-leaflet';
import { Tooltip } from 'react-leaflet';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { drillData, firstFetch, directorateData } from "../redux/actions/coOrdinateActions";


class GeojsonLayer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  myStyle = () => {
    return {
      color: "white",
      weight: 1,
      opacity: .8,
      fillColor: "green",
      dashArray: '8 5',
      fillOpacity: .7
    }
  }

  drillInside = (divId, zillaId) => () => {
    this.props.drillData(divId, zillaId);
    this.props.directorates.length && this.props.directorateData(divId, zillaId)
  };


  componentWillMount() {
    this.props.firstFetch();
  }


  render() {
    return (
      <FeatureGroup>
        {
          this.props.coOrdData && this.props.coOrdData.map(f => {
            if (this.props.directorates && f.properties.divid && f.properties.zillaid === undefined) {
              const found = this.props.directorates.find(element => element.divid === f.properties.divid);
              if (found !== undefined) f.properties.doctorateActive = found.totalActive;
            }
            else if (f.properties.divid && f.properties.zillaid) {
              const found = this.props.directorates.find(element => element.zillaid === f.properties.zillaid);
              if (found !== undefined) f.properties.doctorateActive = found.totalActive;
            }

            return <GeoJSON key={f.properties.gid} data={f} style={this.myStyle} onClick={this.drillInside(f.properties.divid, f.properties.zillaid)}>
              <Tooltip
                className='ToolTipProvince'
                style={this.toolStyle}
                direction="center"
                stroke={false}
                permanent>
                <span className="toolTipOutSide">
                  {f.properties.divisionen ? f.properties.divisionen : f.properties.zillanamee ? f.properties.zillanamee : f.properties.upazilanam ? f.properties.upazilanam : null}

                </span>
              </Tooltip>
              {
                f.properties.doctorateActive &&
                <Tooltip direction="bottom" permanent>
                  <span className="toolTipInside">
                    {f.properties.doctorateActive ? f.properties.doctorateActive : null}
                  </span>
                </Tooltip>
              }
              {/* <Popup>{f.properties.divisionen}</Popup> */}
            </GeoJSON>
          })}
      </FeatureGroup>
    );
  }
}


const mapStateToProps = state => ({
  coOrdData: state.mapData.coordinate,
  mapPosition: state.mapData.position,
  directorates: state.mapData.directorate
});

export default connect(mapStateToProps, { firstFetch, drillData, directorateData })(GeojsonLayer);